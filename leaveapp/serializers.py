from rest_framework import serializers
from .models import Employee, LeaveRequest


class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Employee
        fields = ['id', 'name', 'department', 'manager', 'leave_balance', 'is_manager', 'employee', 'requests']


class LeaveRequestSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LeaveRequest
        fields = ['id', 'employee', 'reason', 'start_date', 'end_date', 'date', 'is_approved']
