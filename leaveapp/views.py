from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from leaveapp.models import Employee, LeaveRequest
from leaveapp.serializers import EmployeeSerializer, LeaveRequestSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class LeaveRequestViewSet(viewsets.ModelViewSet):
    queryset = LeaveRequest.objects.all()
    serializer_class = LeaveRequestSerializer