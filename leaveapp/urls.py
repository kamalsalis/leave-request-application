from django.urls import path, include
from rest_framework import routers

from leaveapp.views import EmployeeViewSet, LeaveRequestViewSet

router = routers.DefaultRouter()
router.register(r'employees', EmployeeViewSet)
router.register(r'requests', LeaveRequestViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]