from django.db import models


# Create your models here.
class Employee(models.Model):
    name = models.CharField(max_length=200)
    department = models.CharField(max_length=200)
    manager = models.ForeignKey('self', null=True, blank=True, related_name='employee', on_delete=models.CASCADE)
    leave_balance = models.IntegerField()
    is_manager = models.BooleanField()

    def __str__(self):
        return self.name


class LeaveRequest(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='requests',)
    reason = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField()
    date = models.DateField(auto_now=True)
    is_approved = models.BooleanField()

    def __str__(self):
        return self.employee.name
